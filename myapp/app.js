var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

var nodemailer = require("nodemailer");

var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "test.message.home.work@gmail.com",
        pass: "test3344"
    }
});


// view engine setup
//app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

 
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', routes);
app.use('/users', users);


app.use('/sendmessage', function(req, res){

var testmessage = "";

if(req.query.message!=null){
testmessage = req.query.message;
}
 
var fs = require('fs');
var emails = fs.readFileSync('./emails').toString().split("\n");

console.log('test message is ' + testmessage);

emails.forEach(function(value){
  

var mailOptions={
   to : value,
   subject : "Message from Expedia Sample Test",
   text : testmessage
}
console.log(mailOptions);
smtpTransport.sendMail(mailOptions, function(error, response){
if(error){
console.log(error);
}else{
console.log("Message sent: " + response.message);
}
});

});
 


res.redirect('/');

});



app.use('/registeremail', function(req, res){

 
var fs = require('fs');

var emails = fs.readFileSync('./emails').toString().split("\n");

if(req.query.email!=null){
if(emails.indexOf(req.query.email)<0){
emails.push(req.query.email);
}
}

fs.writeFileSync("./emails", "");

for( var j = 0; j < emails.length; j++){
console.log('email ' + j + ' is ' + emails[j]);
}

for(var i = 0; i < emails.length; i++){
  

fs.appendFileSync("./emails", emails[i]); 
 


}

res.redirect("/register.html");

}
);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});



// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
