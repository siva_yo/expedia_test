1. You can register for the messages at http://52.72.255.24/register.html 
   with your email address 

2. You can broadcast messages to all registered users' email addresses at 
   http://52.72.255.24/generator.html 

3. The test script is written in Python and is located in the test folder
   
4. To run the test script from terminal, use the following syntax 
   python testscript.py "message to be broadcasted"

5. Thank you for your time.  

